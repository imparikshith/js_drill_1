const inventory = require('../data.js');

function problem3(){
    const sortedCarModels = inventory.reduce(function(carModels,curr){
        carModels.push(curr.car_model.toUpperCase());
        return carModels;
    },[]).sort();
    return sortedCarModels;
}
module.exports=problem3;