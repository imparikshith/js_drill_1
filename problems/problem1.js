const inventory = require('../data.js');

function problem1()
{
    const output = inventory.reduce(function(index,curr){
        if(curr.id == 33)
        {
            index = curr;
        }
        return index;
    },inventory[0])
    console.log("Car 33 is a " + output.car_year + " " + output.car_make + " " + output.car_model);
}
module.exports=problem1;