const inventory = require('../data.js');

function problem4(){
    const carYears=inventory.reduce(function(year,curr){
        year.push(curr.car_year);
        return year;
    },[])
    return carYears;
}
module.exports=problem4;