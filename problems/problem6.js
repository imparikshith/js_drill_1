const inventory = require('../data.js');

function problem6(){
    const BMWAndAudi=inventory.filter((car)=>car.car_make=='Audi'||car.car_make=='BMW');
    return BMWAndAudi;
}
module.exports=problem6;