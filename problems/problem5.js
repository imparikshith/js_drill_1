var problem4=require('./problem4.js');

const years=problem4();
function problem5(){
    const oldCars=years.reduce(function(carYear,curr){
        if(curr<2000)
        {
            carYear.push(curr);
        }
        return carYear;
    },[])
    return oldCars;
}
module.exports=problem5;