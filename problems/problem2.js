const inventory = require('../data.js');

function problem2(){
    const maxIdCar = inventory.reduce(function(lastCar, currCar){
        if (currCar.id > lastCar.id) {
            return currCar;
        }
    });
    console.log("Last car is a " + maxIdCar.car_make + " " + maxIdCar.car_model);
}
module.exports=problem2;